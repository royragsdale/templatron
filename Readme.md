# Templatron

[![pipeline status][pimg]][plnk] [![coverage report][cimg]][clnk] [![Go Report Card][rimg]][rlnk] [![GoDoc][dimg]][dlnk]

A simple, highly opinionated, library to load and render go html/template
templates. This library focuses on providing a mechanism for composing a Page
from a collection of base templates, layouts and components. It does this is the
fashion demonstrated by the go documentation Template Share [example][share]
and is inspired by this excellent stack overflow [response][so].

This library also supports registering a specific struct type with a Page to
allow for type checking and validation.

## Template loading

Here's how your templates are loaded.

1. All the templates are parsed into a single common namespace.
2. Your pages specific template is re-parsed last so it's definitions take
   precedence.


[so]:https://stackoverflow.com/a/11468132
[share]:https://golang.org/pkg/text/template/#example_Template_share

[pimg]:https://gitlab.com/royragsdale/templatron/badges/master/pipeline.svg
[plnk]:https://gitlab.com/royragsdale/templatron/commits/master

[cimg]:https://gitlab.com/royragsdale/templatron/badges/master/coverage.svg
[clnk]:https://gitlab.com/royragsdale/templatron/commits/master

[rimg]:https://goreportcard.com/badge/gitlab.com/royragsdale/templatron
[rlnk]:https://goreportcard.com/report/gitlab.com/royragsdale/templatron

[dimg]:https://godoc.org/gitlab.com/royragsdale/templatron?status.svg
[dlnk]:https://godoc.org/gitlab.com/royragsdale/templatron


