/*
Package templatron implements a simple library for loading and rendering
html/templates templates.

The library provides a mechanism for composing a page from a collation of base
templates, layouts and components

*/
package templatron

import (
	"bytes"
	"fmt"
	"html/template"
	"io"
	"os"
	"path/filepath"
	"strings"
)

// Templatron defines the external interface for all view operations.
type Templatron interface {
	RegisterPage(name string, templateName string) error
	Render(w io.Writer, name string, data interface{}) error
}

// templatron is the internal backing data structure that maps a registered
// page to a parsed template.
type templatron struct {
	root      string
	common    *template.Template
	templates map[string]*template.Template
	pages     map[string]page
}

// page is the primary organizing structure for this library. Everything is
// structured around the use case of finalizing a specific composition of other
// templates (eg: base, layout, components) into a page.
type page struct {
	template *template.Template
}

const (
	parsingCommonErr = "Error: parsing common templates failed with: %v"
	baseNotFoundErr  = "Error: base template not found, ensure it exists somewhere in templateRoot"
	pageNotFoundErr  = "Error: page not registered"
	templateParseErr = "Error: parsing template : %v"
	cloneErr         = "Error: cloning common templates: %v"
)

// LoadTemplates loads and parses (recursively) all .html files in the
// templateRoot into a common namespace.  Each template can then be registered
// as a page to override the default definitions and then be rendered.
// baseTemplate is the main template that will get executed during Render. It
// can be a file name or a template that is defined.
func LoadTemplates(templateRoot string, baseTemplate string) (Templatron, error) {

	// Create backing data structures
	templates := map[string]*template.Template{}
	pages := map[string]page{}
	tron := templatron{root: templateRoot, templates: templates, pages: pages}

	// Recursively parse all ".html" files in templateRoot into the common template set
	common := template.New(baseTemplate)
	err := recursivelyAdd(templateRoot, common)
	if err != nil {
		return tron, fmt.Errorf(parsingCommonErr, err)
	}

	// Ensure base template exits, required for Execute in Render Need to check
	// defined because we explicitly name it during the creation so lookup
	// succeeds even if it has no definition.
	if !strings.Contains(common.DefinedTemplates(), baseTemplate) {
		return tron, fmt.Errorf(baseNotFoundErr)
	}
	tron.common = common

	return tron, nil
}

// RegisterPage maps a specific page name and dataType to a templateFile. The
// common templates loaded in LoadTemplate are cloned and then the templateFile
// is parsed so it's definitions take precedence and complete the page's
// template. templateFile can be an absolute path, or relative to the
// templateRoot specified.
func (tron templatron) RegisterPage(name string, templateFile string) error {
	// Copy the common templates into a page specific namespace
	complete, err := tron.common.Clone()
	if err != nil {
		return fmt.Errorf(cloneErr, err)
	}

	// Get path to template file
	if !filepath.IsAbs(templateFile) {
		templateFile = filepath.Join(tron.root, templateFile)
	}

	// If templateFile has already been parsed, use it, otherwise parse it to
	// override definition.
	tmpl, exists := tron.templates[templateFile]
	if !exists {
		tmpl, err = complete.ParseFiles(templateFile)
		if err != nil {
			return fmt.Errorf(templateParseErr, err)
		}
		tron.templates[templateFile] = tmpl
	}

	// Store mapping from page to template so that it can be rendered
	tron.pages[name] = page{tmpl}
	return nil
}

// Render looks up a page by name and executes that template with the provided
// data. It uses a buffer so that it can catch any errors prior to writting to
// (what is likely) an HTTP response writter. Buffer technique per:
// https://stackoverflow.com/questions/30821745/idiomatic-way-to-handle-template-errors-in-golang
func (tron templatron) Render(w io.Writer, name string, data interface{}) error {

	// Lookup page by name
	page, ok := tron.pages[name]
	if !ok {
		return fmt.Errorf(pageNotFoundErr)
	}

	// Create a buffer to render to
	buf := &bytes.Buffer{}
	err := page.template.Execute(buf, data)
	if err != nil {
		// Pass error back up (having not written to the writer)
		return err
	}

	// No error, send the content, HTTP 200 response status implied
	_, err = buf.WriteTo(w)
	return err
}

// recursivelyAdd takes a path and Parses all ".html" files into the existing
// template. This is used to build up the common templates.
// ref: https://stackoverflow.com/a/38688083
func recursivelyAdd(dir string, tmpl *template.Template) error {
	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		if strings.HasSuffix(path, ".html") {
			_, err = tmpl.ParseFiles(path)
			if err != nil {
				return err
			}
		}
		return nil
	})
	return err
}
