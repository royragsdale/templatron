package templatron

import (
	"bytes"
	"log"
	"os"
	"testing"

	"github.com/stretchr/testify/assert"
)

// Global resources for pkg tests
var renderer Templatron // test public interfaces
var tron templatron     // test internal functionality

func TestMain(m *testing.M) {
	var err error

	// Load templates for use testing the public interface
	renderer, err = LoadTemplates("./test/templates", "base.html")
	if err != nil {
		log.Fatalf("Unexpected Error: loading templates: %v", err)
	}

	// Load templates for use testing internal functionality
	temp, err := LoadTemplates("./test/templates", "base.html")
	if err != nil {
		log.Fatalf("Unexpected Error: loading templates: %v", err)
	}
	tron = temp.(templatron) //cast for testing

	// run tests
	os.Exit(m.Run())
}

func TestRegisterPage(t *testing.T) {

	// register a page
	page1 := "p1"
	templateName := "p1.html"
	assert.Nil(t, tron.RegisterPage(page1, templateName), "registering a valid page")
	assert.Contains(t, tron.pages, page1, "page should exist after registration")

	// Regsiter another page with the same template
	page2 := "anotherPage"
	assert.Nil(t, tron.RegisterPage(page2, templateName), "register the same template to multiple pages")
	assert.Contains(t, tron.pages, page2, "page should exist after registration")

	assert.Equal(t, tron.pages[page1].template, tron.pages[page2].template, "templates should match")

	// Re-register a page with a different template (also with a template outside templateRoot and common set)
	newTemplate := "../external/p3.html"
	assert.Nil(t, tron.common.Lookup("only-in-p3"), "ensure not defined prior to registration")
	assert.Nil(t, tron.RegisterPage(page2, newTemplate), "re-registering a valid page")
	assert.Contains(t, tron.pages, page2, "page should exist after registration")
	assert.NotNil(t, tron.pages[page2].template.Lookup("only-in-p3"), "re-registration should update definitions")

	// Register a non-existent page
	err := tron.RegisterPage(page1, "non-existent-template")
	assert.NotNil(t, err, "register invalid page should fail")
	assert.Contains(t, err.Error(), "no such file", "check sane error no non existent template")

}

func TestLoadTemplates(t *testing.T) {

	// Check eror conditions
	_, err := LoadTemplates("./test/templates", "non-existent-base")
	assert.EqualErrorf(t, err, baseNotFoundErr, "invalid error for non existent base template")

	_, err = LoadTemplates("./test/emptydir", "base.html")
	assert.EqualErrorf(t, err, baseNotFoundErr, "invalid error for non existent base template")

	_, err = LoadTemplates("./test/badtemplate", "base.html")
	assert.NotNil(t, err, "should error on invalid template")
	assert.Contains(t, err.Error(), "parsing common templates failed")

	// check valid loads
	_, err = LoadTemplates("./test/templates", "alt-base")
	assert.Nil(t, err, "using baseTemplate defined in block should be valid")

	mixed, err := LoadTemplates("./test/mixed", "base.html")
	assert.Nil(t, err, "unexpected error in a valid load")
	assert.Nil(t, mixed.(templatron).common.Lookup("base.txt"), "only html templates should be loaded")
}

func TestRender(t *testing.T) {

	type P1 struct{}

	// render it
	w := new(bytes.Buffer)
	err := tron.Render(w, "p1", P1{})
	assert.Nil(t, err, "error on valid render of p1")
	assert.Contains(t, w.String(), "p1 content", "correctly rendered page content")
	assert.Contains(t, w.String(), "base content", "correctly rendered base content")

	err = tron.Render(w, "not-a-registered-page", P1{})
	assert.EqualError(t, err, pageNotFoundErr, "can't render an non registered page")

}
